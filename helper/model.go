package helper

import (
	"bitbucket.org/adecrisna977/dashboard-users-management/model/domain"
	"bitbucket.org/adecrisna977/dashboard-users-management/model/web"
)

func ToUserResponse(user domain.Users) web.UserResponse {
	return web.UserResponse{
		UserId:         user.UserId,
		FullName:       user.FullName,
		PersonalNumber: user.PersonalNumber,
		Password:       user.Password,
		Email:          user.Email,
		Status:         user.Status,
		RoleId:         user.RoleId,
		CreatedAt:      user.CreatedAt,
		CreatedBy:      user.CreatedBy,
		ActivedAt:      user.ActivedAt,
		ActivedBy:      user.ActivedBy,
		UpdatedAt:      user.UpdatedAt,
		UpdatedBy:      user.UpdatedBy,
	}
}

func ToUserResponses(users []domain.Users) []web.UserResponse {
	var userResponses []web.UserResponse
	for _, user := range users {
		userResponses = append(userResponses, ToUserResponse(user))
	}
	return userResponses
}
