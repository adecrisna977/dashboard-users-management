package repository

import (
	"context"
	"database/sql"

	"bitbucket.org/adecrisna977/dashboard-users-management/model/domain"
)

type UserRepository interface {
	Save(ctx context.Context, tx *sql.Tx, user domain.Users) domain.Users
	Update(ctx context.Context, tx *sql.Tx, user domain.Users) domain.Users
	Delete(ctx context.Context, tx *sql.Tx, user domain.Users)
	FindById(ctx context.Context, tx *sql.Tx, user_id int) (domain.Users, error)
	FindAll(ctx context.Context, tx *sql.Tx) []domain.Users
}
