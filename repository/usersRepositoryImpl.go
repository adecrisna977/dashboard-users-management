package repository

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"bitbucket.org/adecrisna977/dashboard-users-management/helper"
	"bitbucket.org/adecrisna977/dashboard-users-management/model/domain"
)

type UserRepositoryImpl struct {
}

func NewUserRepository() UserRepository {
	return &UserRepositoryImpl{}
}

func (repository *UserRepositoryImpl) Save(ctx context.Context, tx *sql.Tx, user domain.Users) domain.Users {
	SQL := "insert into users(full_name,personal_number,password,email,status,role_id,created_at,created_by,actived_at,actived_by,updated_at,updated_by) values (?,?,?,?,?,?,?,?,?,?,?,?)"
	result, err := tx.ExecContext(ctx, SQL, user.FullName, user.PersonalNumber, user.Password, user.Email, user.Status, user.RoleId, time.Now(), user.CreatedBy, time.Now(), user.ActivedBy, time.Now(), user.UpdatedBy)
	helper.PanicIfError(err)

	id, err := result.LastInsertId()
	helper.PanicIfError(err)

	user.UserId = int(id)
	return user
}

func (repository *UserRepositoryImpl) Update(ctx context.Context, tx *sql.Tx, user domain.Users) domain.Users {
	SQL := "update users set full_name = ?, personal_number = ?, password = ?, email = ?, status = ? , role_id = ?, updated_at = ?,updated_by = ? where user_id = ?"
	_, err := tx.ExecContext(ctx, SQL, user.FullName, user.PersonalNumber, user.Password, user.Email, user.Status, user.RoleId, time.Now(), user.UpdatedBy, user.UserId)
	helper.PanicIfError(err)

	return user
}

func (repository *UserRepositoryImpl) Delete(ctx context.Context, tx *sql.Tx, user domain.Users) {
	SQL := "delete from users where user_id = ?"
	_, err := tx.ExecContext(ctx, SQL, user.UserId)
	helper.PanicIfError(err)

}

func (repository *UserRepositoryImpl) FindById(ctx context.Context, tx *sql.Tx, userId int) (domain.Users, error) {
	SQL := "select user_id, full_name,personal_number,password,email,status,role_id,created_at,created_by,actived_at,actived_by,updated_at,updated_by from users where user_id = ?"
	rows, err := tx.QueryContext(ctx, SQL, userId)
	helper.PanicIfError(err)
	defer rows.Close()

	user := domain.Users{}
	if rows.Next() {
		err := rows.Scan(&user.UserId, &user.FullName, &user.PersonalNumber, &user.Password, &user.Email, &user.Status, &user.RoleId, &user.CreatedAt, &user.CreatedBy, &user.ActivedAt, &user.ActivedBy, &user.UpdatedAt, &user.UpdatedBy)
		helper.PanicIfError(err)
		return user, nil
	} else {
		return user, errors.New("users is not found")
	}
}

func (repository *UserRepositoryImpl) FindAll(ctx context.Context, tx *sql.Tx) []domain.Users {
	SQL := "select user_id, full_name,personal_number,password,email,status,role_id,created_at,created_by,actived_at,actived_by,updated_at,updated_by from users"
	rows, err := tx.QueryContext(ctx, SQL)
	helper.PanicIfError(err)
	defer rows.Close()

	var users []domain.Users
	for rows.Next() {
		user := domain.Users{}
		err := rows.Scan(&user.UserId, &user.FullName, &user.PersonalNumber, &user.Password, &user.Email, &user.Status, &user.RoleId, &user.CreatedAt, &user.CreatedBy, &user.ActivedAt, &user.ActivedBy, &user.UpdatedAt, &user.UpdatedBy)
		helper.PanicIfError(err)
		users = append(users, user)
	}
	return users
}
