package handler

import (
	"net/http"
	"strconv"

	"bitbucket.org/adecrisna977/dashboard-users-management/helper"
	"bitbucket.org/adecrisna977/dashboard-users-management/model/web"
	"bitbucket.org/adecrisna977/dashboard-users-management/services"
	"github.com/julienschmidt/httprouter"
)

type UserHandlerImpl struct {
	UserService services.UserService
}

func NewUserHandler(userService services.UserService) UserHandler {
	return &UserHandlerImpl{
		UserService: userService,
	}
}

func (handler *UserHandlerImpl) Create(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {

	userCreateRequest := web.UserCreateRequest{}
	helper.ReadFromRequestBody(request, &userCreateRequest)

	userResponse := handler.UserService.Create(request.Context(), userCreateRequest)
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   userResponse,
	}

	helper.WriteToResponseBody(writer, webResponse)
}

func (handler *UserHandlerImpl) Update(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	userUpdateRequest := web.UserUpdateRequest{}
	helper.ReadFromRequestBody(request, &userUpdateRequest)

	userId := params.ByName("user_id")
	id, err := strconv.Atoi(userId)
	helper.PanicIfError(err)

	userUpdateRequest.UserId = id

	userResponse := handler.UserService.Update(request.Context(), userUpdateRequest)
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)

	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   userResponse,
	}

	helper.WriteToResponseBody(writer, webResponse)
}

func (handler *UserHandlerImpl) Delete(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	userId := params.ByName("user_id")
	id, err := strconv.Atoi(userId)
	helper.PanicIfError(err)

	handler.UserService.Delete(request.Context(), id)
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
	}

	helper.WriteToResponseBody(writer, webResponse)
}

func (handler *UserHandlerImpl) FindById(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	userId := params.ByName("user_id")
	id, err := strconv.Atoi(userId)
	helper.PanicIfError(err)

	userResponse := handler.UserService.FindById(request.Context(), id)
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	webResponse := web.WebResponse{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   userResponse,
	}

	helper.WriteToResponseBody(writer, webResponse)
}

func (handler *UserHandlerImpl) FindAll(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	userResponses := handler.UserService.FindAll(request.Context())
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	webResponse := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   userResponses,
	}

	helper.WriteToResponseBody(writer, webResponse)
}
