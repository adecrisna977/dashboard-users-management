package services

import (
	"context"
	"database/sql"

	"bitbucket.org/adecrisna977/dashboard-users-management/exception"
	"bitbucket.org/adecrisna977/dashboard-users-management/helper"
	"bitbucket.org/adecrisna977/dashboard-users-management/model/domain"
	"bitbucket.org/adecrisna977/dashboard-users-management/model/web"
	"bitbucket.org/adecrisna977/dashboard-users-management/repository"
	"github.com/go-playground/validator/v10"
)

type UserServiceImpl struct {
	UserRepository repository.UserRepository
	DB             *sql.DB
	Validate       *validator.Validate
}

func NewUserService(userRepository repository.UserRepository, DB *sql.DB, validate *validator.Validate) UserService {
	return &UserServiceImpl{
		UserRepository: userRepository,
		DB:             DB,
		Validate:       validate,
	}
}

func (service *UserServiceImpl) Create(ctx context.Context, request web.UserCreateRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	user := domain.Users{
		FullName:       request.FullName,
		PersonalNumber: request.PersonalNumber,
		Password:       request.Password,
		Email:          request.Email,
		Status:         request.Status,
		RoleId:         request.RoleId,
		CreatedBy:      request.CreatedBy,
		ActivedBy:      request.CreatedBy,
		UpdatedBy:      request.UpdatedBy,
	}

	user = service.UserRepository.Save(ctx, tx, user)
	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) Update(ctx context.Context, request web.UserUpdateRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	user, err := service.UserRepository.FindById(ctx, tx, request.UserId)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	user.FullName = request.FullName
	user.PersonalNumber = request.PersonalNumber
	user.Password = request.Password
	user.Email = request.Email
	user.Status = request.Status
	user.RoleId = request.RoleId
	user.UpdatedBy = request.UpdatedBy

	user = service.UserRepository.Update(ctx, tx, user)
	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) Delete(ctx context.Context, user_id int) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	user, err := service.UserRepository.FindById(ctx, tx, user_id)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	service.UserRepository.Delete(ctx, tx, user)
}

func (service *UserServiceImpl) FindById(ctx context.Context, user_id int) web.UserResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	user, err := service.UserRepository.FindById(ctx, tx, user_id)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	return helper.ToUserResponse(user)

}

func (service *UserServiceImpl) FindAll(ctx context.Context) []web.UserResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	users := service.UserRepository.FindAll(ctx, tx)

	return helper.ToUserResponses(users)
}
