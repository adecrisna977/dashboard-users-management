package app

import (
	"database/sql"

	"bitbucket.org/adecrisna977/dashboard-users-management/helper"
)

func NewDB() *sql.DB {
	db, err := sql.Open("mysql", "root@tcp(localhost:3306)/administrator?parseTime=true")
	helper.PanicIfError(err)

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(20)

	return db
}
