package main

import (
	"net/http"

	"bitbucket.org/adecrisna977/dashboard-users-management/app"
	"bitbucket.org/adecrisna977/dashboard-users-management/exception"
	"bitbucket.org/adecrisna977/dashboard-users-management/handler"
	"bitbucket.org/adecrisna977/dashboard-users-management/helper"
	"bitbucket.org/adecrisna977/dashboard-users-management/repository"
	"bitbucket.org/adecrisna977/dashboard-users-management/services"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
)

func main() {

	db := app.NewDB()
	validate := validator.New()
	userRepository := repository.NewUserRepository()
	userService := services.NewUserService(userRepository, db, validate)
	userHandler := handler.NewUserHandler(userService)

	router := httprouter.New()

	router.GET("/api/users", userHandler.FindAll)
	router.GET("/api/users/:user_id", userHandler.FindById)
	router.POST("/api/users", userHandler.Create)
	router.PUT("/api/users/:user_id", userHandler.Update)
	router.DELETE("/api/users/:user_id", userHandler.Delete)

	router.PanicHandler = exception.ErrorHandler

	server := http.Server{
		Addr:    "localhost:3000",
		Handler: router,
	}

	err := server.ListenAndServe()
	helper.PanicIfError(err)
}
