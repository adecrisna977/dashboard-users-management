package web

type UserCreateRequest struct {
	FullName       string `validate:"required,max=200,min=1" json:"full_name"`
	PersonalNumber string `validate:"required,max=15,min=1" json:"personal_number"`
	Password       string `validate:"required,max=220,min=1" json:"password"`
	Email          string `validate:"required,max=100,min=1" json:"email"`
	Status         string `validate:"required,max=15,min=1" json:"status"`
	RoleId         int    `validate:"required" json:"role_id"`

	CreatedBy string `validate:"required,max=100,min=1" json:"created_by"`

	ActivedBy string `validate:"required,max=100,min=1" json:"actived_by"`

	UpdatedBy string `validate:"required,max=100,min=1" json:"updated_by"`
}
