package domain

import "time"

type Users struct {
	UserId         int       `json:"user_id"`
	FullName       string    `json:"full_name"`
	PersonalNumber string    `json:"personal_number"`
	Password       string    `json:"password"`
	Email          string    `json:"email"`
	Status         string    `json:"status"`
	RoleId         int       `json:"role_id"`
	CreatedAt      time.Time `json:"created_at"`
	CreatedBy      string    `json:"created_by"`
	ActivedAt      time.Time `json:"actived_at"`
	ActivedBy      string    `json:"actived_by"`
	UpdatedAt      time.Time `json:"updated_at"`
	UpdatedBy      string    `json:"updated_by"`
}
