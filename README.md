
## Installing GRPC Requirement
- Install protoc-gen-go  `go get -u github.com/golang/protobuf/protoc-gen-go`
- Install grpc-gateway  `go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gatewa`
- Install protoc-gen-swagger `go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger`

setuping command: 
cd proto && protoc --proto_path=. {{$proto_path}}/{{$proto_filename}}.proto --go_out=plugins=grpc:./ --grpc-gateway_out=:./ --swagger_out=:../swagger && cd ..

example for real proto:
cd proto && protoc --proto_path=. v1/example.proto --go_out=plugins=grpc:./ --grpc-gateway_out=:./ --swagger_out=:../swagger && cd ..

if proto folder not found mkdir proto


## Learning Clean Architecture 
- link: https://medium.com/golangid/mencoba-golang-clean-architecture-c2462f355f41

## Golang database with gorm
- link: https://gorm.io/docs/query.html

